<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            $this->db = new PDO('mysql:host=localhost;dbname=test;', 'root', '');  // Create PDO connection
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
		
		try {
			//retrieving all books from the db and creating books in array booklist
		foreach($this->db->query("SELECT * FROM book") as $book) {
			$booklist[$book['id']] = new Book($book['title'], $book['author'],
											$book['description'], $book['id']);
			}
		}
		catch(PDOEXCEPTION $pdoe) {
			echo $pdoe->getMessage();	//if something goes wrong a message will be printed 
		} 
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$book = null;
		
		try {	
			//checking if id contains a numeric value
			if (isset($id) AND is_numeric($id)) {
				$stmt = $this->db->prepare('SELECT * FROM book WHERE id = ?');	//to prevent sql injections
				$stmt->execute(array($id));
				
				if($temp = $stmt->fetch(PDO::FETCH_ASSOC)) {		//fetching data from book
				$book = new Book($temp['title'], $temp['author'],
							  $temp['description'], $temp['id']);	
					}
					return $book;
				} else {
					$view = new errorView();	// error page
					$view->create();
					return null;
					}
		} 
		catch(PDOEXCEPTION $pdoe) {
			echo $pdoe->getMessage(); 
		}
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		try {
			//making sure title and author isn't empty, aswell as correct id
			if ($book->title != '' AND $book->author != '' AND isset($book->id) AND is_numeric($book->id)) {
				if ($book->description == '') {	
					$book->description = null;	//asserting value to NULL
				}
			$stmt = $this->db->prepare("INSERT INTO book (id, title, author, description) VALUES(:id, :title, :author, :description)");
			$stmt->execute(array(':id' => null, ':title' => $book->title, ':author' => $book->author, ':description' => $book->description));
			//$book->id = $this->db->lastInsert();		this gives an error message
			} else {
				$view = new errorView();
				$view->create();
				}
		}
		catch(PDOEXCEPTION $pdoe) {
			echo $pdoe->getMessage(); 
		} 
		//$stmt = "INSERT INTO book (id, title, author, description) VALUES('NULL', '$book->title', '$book->author', '$book->description')";
		//$this->db->exec($stmt);
		
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		if ($book->title != '' AND $book->author != '' AND isset($book->id) AND is_numeric($book->id)) {
				if ($book->description == '') {
					$book->description = null;
				}
		$stmt = $this->db->prepare("UPDATE book SET title = ?, author = ?, description = ? WHERE id = ?");	//correct book id
		$stmt->execute(array($book->title, $book->author, $book->description, $book->id));
		} else {
			$view = new errorView();
					$view->create();
		}
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		if (isset($id) AND is_numeric($id)) {
		$stmt = $this->db->prepare("DELETE FROM book WHERE id=:id");
		$stmt->bindValue(':id', $id, PDO::PARAM_INT); //binds value to id
		$stmt->execute();
		} else {
			$view = new errorView();
					$view->create();
		}
    }
}

?>